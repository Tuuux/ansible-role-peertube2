help:
	@echo "Ansible role Peertube "
	@echo ""
	@echo "USAGE"
	@echo "	make env - Create the virtual environement"
	@echo "	make requirements - Install requirements"

tests/syntax:
	@echo "Make: Install requirements"
	. venv/bin/activate ;\
	cd tests;\
	ansible-playbook -i inventory --syntax-check test_ci.yml

tests/roles:
	@echo "Make: Install requirements"
	. venv/bin/activate ;\
	cd tests;\
	ansible-playbook -i inventory test_ci.yml

requirements: | env
	@echo "Make: Install requirements"
	. venv/bin/activate ;\
	pip install -U -r requirements.txt

env:
	@echo "Make: Create Virtual Environement"
	python3 -m venv venv
	. venv/bin/activate ;\
	pip install -U pip